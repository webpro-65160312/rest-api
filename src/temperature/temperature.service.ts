import { Injectable } from '@nestjs/common';

@Injectable()
export class TemperatureService {
  toFahrenheit(c: number): number {
    return c * (9 / 5) + 32;
  }
}
