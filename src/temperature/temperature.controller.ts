import { Body, Controller, Get, Param, Query, Req } from '@nestjs/common';
import { TemperatureService } from './temperature.service';

@Controller('temperature')
export class TemperatureController {
  constructor(private readonly temperatureService: TemperatureService) {}

  @Get()
  getDefault() {
    return 'Default Temperature';
  }

  @Get('justQuery')
  getCelsiusQuery(@Req() req, @Query('celsius') celsius: string) {
    return 'celsius = ' + parseFloat(celsius);
  }

  @Get('justParam/:celsius')
  getCelsiusParam(@Req() req, @Param('celsius') celsius: string) {
    return { celsius };
  }

  @Get('justBody')
  getCelsiusBody(@Req() req, @Body() celsius: string) {
    return celsius;
  }

  @Get('convert/:celsius')
  getConverted(@Param('celsius') c: string) {
    return {
      Celsius: parseFloat(c),
      Fahrenheit: this.temperatureService.toFahrenheit(parseFloat(c)),
    };
  }
}
