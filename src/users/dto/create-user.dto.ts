export class CreateUserDto {
  login: string;
  password: string;
  roles: ('admin' | 'users')[];
  gender: 'male' | 'female';
}
